using System;
using UnityEngine;

public enum States
{
    Alive,
    Dead
}

[CreateAssetMenu(menuName = "ScriptableObjects/States/PlayerState")]
public class PlayerState : TState<States>
{
    private void OnValidate()
    {
        Value = States.Alive;
    }
}
