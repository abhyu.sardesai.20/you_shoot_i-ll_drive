using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Player : ADamageableObject
{
    [SerializeField]
    private PlayerState _pState;
    [SerializeField]
    private FPSShooting _fpsPlayer;
    [SerializeField]
    private AudioClip _collectionSound;
    [SerializeField]
    private AudioSource _audioSource;
    [SerializeField]
    private UnityEvent onDeathEvents;
    [SerializeField]
    private HealthState _healthStates;

    private PrometeoCarController _carController;

    private void Start()
    {
        _carController = GetComponent<PrometeoCarController>();
        _healthStates.Value = _health;
    }

    private void OnCollisionEnter(Collision collision)
    {
        var col = collision.gameObject;
        if (col.GetComponent<Health>() != null)
        {
            _health += col.GetComponent<Health>().Count;
        }
        if (col.GetComponent<Ammo>() != null)
        {
            _fpsPlayer.AmmunitionCurrent += col.GetComponent<Health>().Count;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        var col = other.gameObject;
        if (col.GetComponent<Health>() != null)
        {
            _audioSource.PlayOneShot(_collectionSound, 0.15f);
            _health += col.GetComponent<Health>().Count;
            _health = _health > 100 ? 100 : _health;
            _healthStates.Value = _health;
            col.SetActive(false);
        }
        if (col.GetComponent<Ammo>() != null)
        {
            _audioSource.PlayOneShot(_collectionSound, 0.15f);
            _fpsPlayer.AmmunitionCurrent += col.GetComponent<Ammo>().Count;
            col.SetActive(false);
        }
    }
    public override void OnDie()
    {
        _carController.Move = false;
        _pState.Value = States.Dead;
        onDeathEvents.Invoke();
    }
    public override void OnHit(float damage)
    {
        base.OnHit(damage);
        _healthStates.Value = _health;
    }

}
