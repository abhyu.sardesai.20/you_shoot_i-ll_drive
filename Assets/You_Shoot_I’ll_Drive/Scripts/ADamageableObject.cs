using UnityEngine;

public abstract class ADamageableObject : MonoBehaviour
{
    [SerializeField]
    protected float _health = 100f;

    public bool IsAlive => _health > 0f;

    
    virtual 
    public void OnHit(float damage)
    {
        _health -= damage;
        if (_health <= 0f)
        {
            _health = 0f;
            OnDie();
        }
    }

    virtual 
    public void OnDie()
    {
        _health = 0;
        Destroy(gameObject);
    }
}