using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{  
    [SerializeField]
    private GameState _gameStates;
    [SerializeField]
    private HealthState _healthStates;

    [SerializeField]
    private GameObject _pausePanel;
    [SerializeField]
    private List<GameObject> _uiBars;


    public UnityEvent OnPause;

    private void OnEnable()
    {
        _gameStates.Observers += ShowPauseMenu;
        _healthStates.Observers += UpdateHealthBar;
        //listening to the player state
        //Subscribing to the observer
    }

    private void UpdateHealthBar(float obj)
    {
        int bars = (int)obj / 10;
        foreach (var bar in _uiBars)
        {
            bar.SetActive(false);
        }
        for (int i = 0; i < bars; i++)
        {
            _uiBars[i].SetActive(true);
        }
    }

    private void OnDisable()
    {
        _gameStates.Observers -= ShowPauseMenu;
        _healthStates.Observers -= UpdateHealthBar;
    }

    private void ShowPauseMenu(GStates obj)
    {
        if (obj == GStates.Pause)
        {
            _pausePanel.SetActive(true);
        }
        
        else if (obj == GStates.Play)
        {
            _pausePanel.SetActive(false);
        }
        
        OnPause?.Invoke();
    }
}