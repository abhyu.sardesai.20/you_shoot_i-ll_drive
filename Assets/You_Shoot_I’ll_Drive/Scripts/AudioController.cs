using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    [SerializeField]
    private List<AudioClip> _audioClips;

    private AudioSource _audioSource;

    [SerializeField]
    private KeyCode _previousTrack = KeyCode.LeftBracket;

    [SerializeField]
    private KeyCode _nextTrack = KeyCode.RightBracket;

    private AudioClip _currentTrack;

    private int trackIndex = 0;

    private bool _playMusic = true;


    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
        trackIndex = 0;
    }

    private void Start()
    {
        _audioSource.Play();
        
    }

    // Update is called once per frame
    void Update()
    {
        print(_audioSource.isPlaying);
        PlayMusic();
        ChangeMusic();

        if (_playMusic)
        {
            _audioSource.Play();
            _playMusic = false;
        }

    }

    private void PlayMusic()
    {
        _audioSource.clip = _audioClips[trackIndex];
        // _audioSource.PlayOneShot(_currentTrack, 0.25f);
    }

    private void ChangeMusic()
    {

        if (Input.GetKeyDown(_previousTrack))
        {
            if (trackIndex != 0)
            {
                trackIndex--;
                _playMusic = true;
            }
        }

        if (Input.GetKeyDown(_nextTrack))
        {
            if (trackIndex != _audioClips.Count)
            {
                trackIndex++;
                _playMusic = true;
            }
        }
    }
}