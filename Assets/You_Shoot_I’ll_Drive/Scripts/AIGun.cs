using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIGun : MonoBehaviour
{
    [SerializeField]
    private GameObjectReferences _objectReferences;
    [SerializeField]
    private Transform _target;
    [SerializeField]
    private GameObject _prefabProjectile;
    [SerializeField]
    private float projectileImpulse = 400.0f;
    [SerializeField]
    private Transform _firingPos;
    [SerializeField]
    private float _range = 15f;
    [SerializeField]
    private Transform _gunToRotate;

    [SerializeField]
    private float _turnSpeed;
    [SerializeField]
    private float _fireRate;
    [SerializeField]
    private AudioClip _firingAudioClip;
    [SerializeField]
    private AudioSource _firingAudioSource;
    private float _fireCountdown;
    private CarAI _carAi;

    private void Awake()
    {
        _carAi = GetComponent<CarAI>();
    }

    // Start is called before the first frame update
    void Start()
    {
        _firingAudioSource = GetComponent<AudioSource>();
        //_firingAudioSource.clip = _firingAudioClip;
        _target = _objectReferences._carLocation;
        _carAi.CustomDestination = _objectReferences._carLocation;
    }

    // Update is called once per frame
    void Update()
    {
        if (_target == null || _carAi.move == false) return;

        Vector3 dir = _target.position - _gunToRotate.position;
        Quaternion lookRotation = Quaternion.LookRotation(dir);
        Vector3 rotation = Quaternion.Lerp(_gunToRotate.rotation, lookRotation, Time.deltaTime * _turnSpeed).eulerAngles;
        _gunToRotate.rotation = Quaternion.Euler(rotation.x, rotation.y, 0f);

        if (_fireCountdown < 0f)
        {
            Shoot(lookRotation);
            _fireCountdown = 1 / _fireRate;
        }

        _fireCountdown -= Time.deltaTime;

    }

    private void Shoot(Quaternion lookRotation)
    {
        
        _firingAudioSource.Play();
        //Spawn projectile from the projectile spawn point.
        GameObject projectile = Instantiate(_prefabProjectile, _firingPos.position, _gunToRotate.rotation);
        //Add velocity to the projectile.
        projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * projectileImpulse;
    }
}
