using System;
using UnityEngine;

public class TState<T> : ScriptableObject
{
    public event Action<T> Observers;
    //Delegate and Variable

    [SerializeField] private T _value;

    public T Value
    {
        get
        {
            return _value;
        }
        
        set
        {
            _value = value;
            Observers?.Invoke(_value);
            //Called everytime the value is set
        }
    }    
}
