﻿using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObjects/States/HealthState")]
public class HealthState :TState<float>
{ }
