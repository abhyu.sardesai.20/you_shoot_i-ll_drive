using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    private PlayerState _playerState;

    [SerializeField]
    private GameState _gameState;

    private void OnEnable()
    {
        _gameState.Observers += CheckGameState;
    }

    private void OnDisable()
    {
        _gameState.Observers -= CheckGameState;

    }

    private void CheckGameState(GStates obj)
    {
        if(obj == GStates.Finished)
        {
            PlayGame();
        }
    }

    private void Awake()
    {
        Time.timeScale = 1;
        _gameState.Value = GStates.Play;
        _playerState.Value = States.Alive;
    }

    private void Update()
    {
        CheckPlayerState(_playerState.Value);
        //Checking for player state "Value"
        if (Input.GetKeyDown(KeyCode.Escape))
            CallingPause();
    }


    public void CheckPlayerState(States obj)
    {
        if (obj == States.Dead)
        {
            RestartGame();
        }
    }

    public void CallingPause()
    {
        if (_gameState.Value == GStates.Play)
        {
            Time.timeScale = 0f;
            _gameState.Value = GStates.Pause;
        }
        else if (_gameState.Value == GStates.Pause)
        {
            Time.timeScale = 1f;
            _gameState.Value = GStates.Play;
        }

    }

    public void PlayGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void MainMenu()
    {
        SceneManager.LoadScene(0);
    }
}