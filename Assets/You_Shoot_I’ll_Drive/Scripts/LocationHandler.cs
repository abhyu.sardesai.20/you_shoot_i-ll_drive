using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LocationHandler : MonoBehaviour
{
    [SerializeField]
    private GameObjectReferences _objectReferences;

    private void Update()
    {
        this.transform.position = _objectReferences._fpsLocation.position; 
    }
}
