using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class FirstPersonController : MonoBehaviour
{
	[Tooltip("Rotation speed of the character")]
    [SerializeField]
	private float _rotationSpeed = 1.0f;

	[Header("Cinemachine")]
	[Tooltip("The follow target set in the Cinemachine Virtual Camera that the camera will follow")]
    [SerializeField]
	private GameObject _cinemachineCameraTarget;

	[Tooltip("How far in degrees can you move the camera up")]
    [SerializeField]
	private float _topClamp = 90.0f;

	[Tooltip("How far in degrees can you move the camera down")]
    [SerializeField]
	private float _bottomClamp = -90.0f;

	[Header("Mouse Cursor Settings")]
	public bool cursorLocked = true;
	public bool cursorInputForLook = true;

    [SerializeField]
	private PlayerState _playerState;
	
	//NEW INPUT SYSTEM
	private AllPlayerInputs _allPlayerInputs;
	private Vector2 _look;
	// cinemachine
	private float _cinemachineTargetPitch;
	private float _rotationVelocity;

	private const float _threshold = 0.01f;
	private bool _alive = true;
	[SerializeField]
	private GameState _gameStateSO;
	private GStates _gStates;

	private void Awake()
    {
		//New Input System
		_allPlayerInputs = new AllPlayerInputs();
	}

    private void OnEnable()
    {
		_allPlayerInputs.Enable();
		_allPlayerInputs.FPS.Look.performed += ctx => _look = ctx.ReadValue<Vector2>();
		_allPlayerInputs.FPS.Look.canceled += ctx => _look = ctx.ReadValue<Vector2>();
		_playerState.Observers += PlayerDead;
		_gameStateSO.Observers += SetGameState;
	}

    private void SetGameState(GStates obj)
    {
		_gStates = obj;
		if(_gStates == GStates.Pause)
        {
			SetCursorState(false);
        }
        else
        {
			SetCursorState(true);
		}
    }

    private void OnDisable()
    {
		_allPlayerInputs.Disable();
		_playerState.Observers -= PlayerDead;
		_gameStateSO.Observers -= SetGameState;
	}

    private void PlayerDead(States obj)
    {
        if(obj == States.Dead)
        {
			_alive = false;
        }
    }

    private void LateUpdate()
    {
		if (_alive ==false) return;
		if (_gStates == GStates.Pause) return;
        CameraRotation();
    }

	private void CameraRotation()
	{
		// if there is an input
		if (_look.sqrMagnitude >= _threshold)
		{
			//Don't multiply mouse input by Time.deltaTime
			float deltaTimeMultiplier = 1.0f;

			_cinemachineTargetPitch += _look.y * _rotationSpeed * deltaTimeMultiplier;
			_rotationVelocity = _look.x * _rotationSpeed * deltaTimeMultiplier;

			// clamp our pitch rotation
			_cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, _bottomClamp, _topClamp);

			// Update Cinemachine camera target pitch
			_cinemachineCameraTarget.transform.localRotation = Quaternion.Euler(_cinemachineTargetPitch, 0.0f, 0.0f);

			// rotate the player left and right
			transform.Rotate(Vector3.up * _rotationVelocity);
		}
	}

	private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
	{
		if (lfAngle < -360f) lfAngle += 360f;
		if (lfAngle > 360f) lfAngle -= 360f;
		return Mathf.Clamp(lfAngle, lfMin, lfMax);
	}

	//private void OnApplicationFocus(bool hasFocus)
	//{
	//	SetCursorState(cursorLocked);
	//}

	private void SetCursorState(bool newState)
	{
		Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
	}
}
