using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Scriptables/ObjectReference")]
public class GameObjectReferences : ScriptableObject
{
    [SerializeField]
    public Transform _fpsLocation;
    [SerializeField]
    public Transform _carLocation;
}
