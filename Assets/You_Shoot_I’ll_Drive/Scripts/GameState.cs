using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;


public enum GStates
{
    Play,
    Pause,
    Quit,
    Finished
}

[CreateAssetMenu(menuName = "ScriptableObjects/States/GameState")]
public class GameState : TState<GStates>
{
    // private void OnValidate()
    // {
    //     Value = GStates.Play;
    // }
}
