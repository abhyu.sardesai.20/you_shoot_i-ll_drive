using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSPlayerLocation : MonoBehaviour
{
    [SerializeField]
    private Transform _fpsPlayerLocation;
    [SerializeField]
    private GameObjectReferences _gameObjectReferences;

    private void Awake()
    {
        _gameObjectReferences._fpsLocation = _fpsPlayerLocation;
        _gameObjectReferences._carLocation = gameObject.transform;
    }
}
