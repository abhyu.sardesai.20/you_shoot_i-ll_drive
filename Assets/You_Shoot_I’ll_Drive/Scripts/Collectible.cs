using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{

    [SerializeField]
    private float _rotationSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0,1f* Time.deltaTime * _rotationSpeed,0 );
    }
}
