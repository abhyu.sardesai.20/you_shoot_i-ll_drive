﻿using UnityEngine;

public abstract class APickups: MonoBehaviour
{
    [SerializeField]
    private int _count;

    public int  Count { get { return _count; } }
    [SerializeField]
    private float _rotationSpeed;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 1f, 0 * Time.deltaTime * _rotationSpeed);
    }
}