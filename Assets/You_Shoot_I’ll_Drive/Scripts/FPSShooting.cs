using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FPSShooting : MonoBehaviour
{
    private AllPlayerInputs _allPlayerInputs;
    [Tooltip("Projectile Prefab. This is the prefab spawned when the weapon shoots.")]
    [SerializeField]
    private GameObject prefabProjectile;
    [SerializeField]
    private GameObject prefabProjectileCasing;
    [Tooltip("How fast the projectiles are.")]
    [SerializeField]
    private float projectileImpulse = 400.0f;   
    [SerializeField]
    private int ammunitionCurrent;
    [SerializeField]
    private int ammunitionMax;
    [SerializeField]
    private int roundsPerMinutes = 200;
    [SerializeField]
    private Transform playerCamera;
    [Tooltip("Maximum distance at which this weapon can fire accurately. Shots beyond this distance will not use linetracing for accuracy.")]
    [SerializeField]
    private float maximumDistance = 500.0f;
    [Tooltip("Mask of things recognized when firing.")]
    [SerializeField]
    private LayerMask mask;
    [SerializeField] 
    private MuzzleBehaviour[] muzzleBehaviours;
    [SerializeField]
    private Transform socketEjection;

    [SerializeField]
    private TextMeshProUGUI _bulletCount;
    [SerializeField]
    private PlayerState _playerState;
    [SerializeField]
    private GameState _gameStateSO;
    private bool holdingButtonFire;
    private float lastShotTime;
    private bool _alive = true;
    public int AmmunitionCurrent { get => ammunitionCurrent; set => ammunitionCurrent = value; }
    private GStates _gStates;
    /// <summary>
    /// Equipped Muzzle Reference.
    /// </summary>


    public bool HasAmmunition() => ammunitionCurrent > 0;
    public float GetRateOfFire() => roundsPerMinutes;

    private void Awake()
    {
        _allPlayerInputs = new AllPlayerInputs();
    }

    private void OnEnable()
    {
        _allPlayerInputs.Enable();
        _allPlayerInputs.FPS.Fire.started += ctx => holdingButtonFire = true;
        _allPlayerInputs.FPS.Fire.canceled += ctx => holdingButtonFire = false;
        _playerState.Observers += PlayerDead;
        _gameStateSO.Observers += SetGameState;
    }
    private void OnDisable()
    {
        _allPlayerInputs.FPS.Fire.started -= ctx => holdingButtonFire = true;
        _allPlayerInputs.FPS.Fire.canceled -= ctx => holdingButtonFire = false;
        _allPlayerInputs.Disable();
        _playerState.Observers -= PlayerDead;
        _gameStateSO.Observers -= SetGameState;
    }

    private void SetGameState(GStates obj)
    {
        _gStates = obj;
    }

    private void PlayerDead(States obj)
    {
        if (obj == States.Dead)
        {
            _alive = false;
        }
    }
    private void Start()
    {       
        _bulletCount.text = ammunitionCurrent.ToString();
    }

    private void Update()
    {
        if (_alive == false) return;
        if (_gStates == GStates.Pause) return;
        Debug.DrawRay(playerCamera.position, playerCamera.forward * 1000.0f, Color.green);
        if (holdingButtonFire)
        {
            if (HasAmmunition())
            {
                //Has fire rate passed.
                if (Time.time - lastShotTime > 60.0f / GetRateOfFire())
                    Fire();
            }
        }
        ammunitionCurrent = ammunitionCurrent > ammunitionMax ? ammunitionMax : ammunitionCurrent;
        _bulletCount.text = ammunitionCurrent.ToString();
    }

    private void Fire()
    {
        lastShotTime = Time.time;

        //We need a muzzle in order to fire this weapon!
        if (muzzleBehaviours == null)
            return;
        //Make sure that we have a camera cached, otherwise we don't really have the ability to perform traces.
        if (playerCamera == null)
            return;
        foreach (var muzzleBehaviour in muzzleBehaviours)
        {
            //Get Muzzle Socket. This is the point we fire from.
            Transform muzzleSocket = muzzleBehaviour.GetSocket();

            //Reduce ammunition! We just shot, so we need to get rid of one!
            ammunitionCurrent = Mathf.Clamp(ammunitionCurrent - 1, 0, ammunitionMax);
            //Play all muzzle effects.
            muzzleBehaviour.Effect();
            //Determine the rotation that we want to shoot our projectile in.
            Quaternion rotation = Quaternion.LookRotation(playerCamera.forward * 1000.0f );
            //If there's something blocking, then we can aim directly at that thing, which will result in more accurate shooting.
            if (Physics.Raycast(playerCamera.position, playerCamera.forward, out RaycastHit hit, maximumDistance, mask))
                rotation = Quaternion.LookRotation(hit.point - muzzleSocket.position);
          
            //Spawn projectile from the projectile spawn point.
            GameObject projectile = Instantiate(prefabProjectile, muzzleSocket.position, rotation);
            //Add velocity to the projectile.
            projectile.GetComponent<Rigidbody>().velocity = projectile.transform.forward * projectileImpulse;
            EjectCasing();
        }
        _bulletCount.text = ammunitionCurrent.ToString();
    }

    public  void EjectCasing()
    {
        //Spawn casing prefab at spawn point.
        if (prefabProjectileCasing != null && socketEjection != null)
            Instantiate(prefabProjectileCasing, socketEjection.position, socketEjection.rotation);
    }
}
