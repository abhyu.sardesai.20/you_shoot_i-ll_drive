using MoreMountains.Feedbacks;
using UnityEngine;

public class GameFeedbacks : MonoBehaviour
{
    [SerializeField]
    private MMFeedbacks _drivingFeedbacks;

    [SerializeField]
    private MMFeedbacks _shootingFeedbacks;

    [SerializeField]
    private MMFeedbacks _collisionFeedbacks;

    [SerializeField]
    private MMFeedbacks _collectibleFeedbacks;

    [SerializeField]
    private MMFeedbacks _boostFeedbacks;
    

    public void PlayDrivingFeedbacks()
    {
        _drivingFeedbacks.PlayFeedbacks();
    }
    
    public void PlayShootingFeedbacks()
    {
        _shootingFeedbacks.PlayFeedbacks();
    }
    
    public void PlayCollisionFeedbacks()
    {
        _collisionFeedbacks.PlayFeedbacks();
    }
    
    public void PlayCollectibleFeedbacks()
    {
        _collectibleFeedbacks.PlayFeedbacks();
    }
    
    public void PlayBoostFeedbacks()
    {
        _boostFeedbacks.PlayFeedbacks();
    }
}
